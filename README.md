# tencent-im-signature

#### 介绍
腾讯IM云通信签名计算

#### 使用
```
package main

import (
	"fmt"
	"./signature"
)

const (
	appID      = 123456
	privateKey = ``
	publicKey  = ``
)

func main() {

	tx := signature.New(appID)

	sign, err := tx.Generate("123", privateKey)
	if err != nil {
		fmt.Println("error:", err)
	}

	if ok, _ := tx.Verify(sign, publicKey); ok {
		fmt.Println("verify success")
	}
}
```